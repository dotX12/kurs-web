<?php

// Change as you required 

return [
    'database' => [
        'host' => '127.0.0.1',
        'port' => '3306',
        'name' => 'crud_mvc_new',
        'username' => 'root',
        'password' => '',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ]
];
